#!/usr/bin/python

from random import randint

def parse(z):
    operators = set("+-*/")
    op_out = []     #Will hold operators found.
    num_out = []    #Will hold non-operators found.
    buff = []
    for d in z:     #One character at a time.
        if d in operators:
            #Operator found, so everything in buff is
            #considered a single number or in this case,
            #a roll. Join and put in num_out.
            num_out.append(''.join(buff))
            buff = []
            op_out.append(d)
        else:
            #No operators here. Just add to the buffer.
            buff.append(d)
    num_out.append(''.join(buff))
    return num_out, op_out
    
print(parse('2d20 + 5d6'))

while True:
    rolls = input("\nPlease enter dice rolls. (i.e. 1d10)\n")
    #print(parse(x))
    
    #figure out how to evaluate one roll at a time and store it
    rolls.split("d")
    times,die = rolls.split("d")
    times = int(times)
    die = int(die)
    if times <= 0:
        print("\nNumber of die to roll is less than or equal to zero.\n")
    else:
        break
print("\nI am rolling " + rolls + "...\n")

sum = 0
for num in range(times):
    result = randint(1,die)
    if num == times - 1:
        print(result, end=" = ")
    else:
        print(result, end=" + ")
    sum = result + sum
print(str(sum) + "\n")